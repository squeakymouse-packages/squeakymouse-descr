#!/usr/bin/python
# -*- coding: utf-8 -*-
# Licensed under the GNU General Public License, version 3.
# See the file http://www.gnu.org/copyleft/gpl.txt

from pisi.actionsapi import get
from os import system
from pisi.actionsapi import pisitools

# if pisi can't find source directory, see /var/pisi/irrlicht/work/ and:
#WorkDir="irrlicht-"+ get.srcVERSION() +"/source/Irrlicht/"

def setup():
    #system("cp -f /root/irrlicht/files/Makefile "+get.curDIR()+"/Makefile")
    print(get.workDIR())

def build():
    system("cd " + get.workDIR() + "/irrlicht-"+ get.srcVERSION() + "/source/Irrlicht/ && make sharedlib libprefix=%s" % get.installDIR() + "/usr/lib/")

def install():
    system("cd " + get.workDIR() + "/irrlicht-"+ get.srcVERSION() + "/source/Irrlicht/ && make install libprefix=%s" % get.installDIR() + "/usr/lib/")

# Take a look at the source folder for these file as documentation.
    system("mkdir -p " + get.installDIR() + "/usr/share/doc/html/irrlicht/ && cp -r " + get.workDIR() + "/irrlicht-" + get.srcVERSION() + "/doc/html/* " + get.installDIR() + "/usr/share/doc/html/irrlicht/")
    pisitools.dodoc("doc/aesGladman.txt", "doc/bzip2-license.txt", "doc/irrlicht-license.txt", "doc/jpglib-license.txt", "doc/libpng-license.txt", "changes.txt", "readme.txt")
# If there is no install rule for a runnable binary, you can 
# install it to binary directory.
#    pisitools.dobin("irrlicht")

# You can use these as variables, they will replace GUI values before build.
# Package Name : irrlicht
# Version : 1.8.4
# Summary : Lightweight graphics library

# For more information, you can look at the Actions API
# from the Help menu and toolbar.
