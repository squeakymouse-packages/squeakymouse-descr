# squeakymouse-descr
Build files and sources for squeakymouse
# Caveats
Pisido has an annoying habit of replacing the install paths of additional files with default paths. If a package uses external files, please do not edit their paths in Pisido or refresh the file list. 
Also, if dsblogoutmgr returns an error at the end of the build process, ignore it and proceed with building the package. The linking command for the executable is seemingly broken in two parts for some reason. The executable builds fine, but bash/make tries to execute it.    
Some patches are rewritten as the original patches refuse to work. I suspect that is because of a change introduced into GNU Patch
