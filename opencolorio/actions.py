#!/usr/bin/python
# -*- coding: utf-8 -*-
# Licensed under the GNU General Public License, version 3.
# See the file http://www.gnu.org/copyleft/gpl.txt

from pisi.actionsapi import cmaketools
from pisi.actionsapi import get
from pisi.actionsapi import pisitools

# if pisi can't find source directory, see /var/pisi/opencolorio/work/ and:
# WorkDir="opencolorio-"+ get.srcVERSION() +"/sub_project_dir/"

def setup():
    cmaketools.configure("-DCMAKE_BUILD_TYPE=release", installPrefix="/usr")

def build():
    cmaketools.make()

def install():
    cmaketools.rawInstall("DESTDIR=%s" % get.installDIR())

# Take a look at the source folder for these file as documentation.
    pisitools.dodoc("ChangeLog", "INSTALL", "LICENSE", "README.md")

# If there is no install rule for a runnable binary, you can 
# install it to binary directory.
#    pisitools.dobin("opencolorio")

# You can use these as variables, they will replace GUI values before build.
# Package Name : opencolorio
# Version : 1.1.0
# Summary : Color management system

# For more information, you can look at the Actions API
# from the Help menu and toolbar.
