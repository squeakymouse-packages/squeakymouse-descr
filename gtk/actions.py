#!/usr/bin/python
# -*- coding: utf-8 -*-
# Licensed under the GNU General Public License, version 3.
# See the file http://www.gnu.org/copyleft/gpl.txt

from pisi.actionsapi import autotools
from pisi.actionsapi import get
from pisi.actionsapi import pisitools

# if pisi can't find source directory, see /var/pisi/gtk/work/ and:
# WorkDir="gtk-"+ get.srcVERSION() +"/sub_project_dir/"

def setup():
    autotools.configure("--prefix=" + get.installDIR()+"/usr --exec-prefix=/usr --includedir=/usr/include")

def build():
    autotools.make("CFLAGS=--std=gnu89")

def install():
    autotools.rawInstall("DESTDIR=%s" % get.installDIR())

# Take a look at the source folder for these file as documentation.
    pisitools.dodoc("ABOUT-NLS", "AUTHORS", "ChangeLog", "ChangeLog.pre-1-0", "COPYING", "HACKING", "INSTALL", "NEWS", "NEWS.pre-1-0", "README", "README.cvs-commits", "TODO")

# If there is no install rule for a runnable binary, you can 
# install it to binary directory.
#    pisitools.dobin("gtk")

# You can use these as variables, they will replace GUI values before build.
# Package Name : gtk
# Version : 1.2.2
# Summary : Legacy version of Glib

# For more information, you can look at the Actions API
# from the Help menu and toolbar.
